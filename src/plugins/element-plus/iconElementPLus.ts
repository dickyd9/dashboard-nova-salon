import { Search } from "@element-plus/icons-vue"

export default {
  install: (app: any) => {
    app.component(Search.name, Search)
  },
}
