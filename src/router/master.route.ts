import CategoryList from "@/pages/master/category/CategoryList.vue"

export default [
  {
    path: "/master/category-list",
    name: "Category List",
    component: CategoryList
  },
  {
    path: "/master/item-category",
    name: "Item Category",
    component: CategoryList
  },
]
